# My GPG keys
tl;dr; Ideally please encrypt stuff to me with as many of the following keys as
possible in order of priority;

* 2351AD620C3D83F2A5A562E17B8C6179AA1CD475
* 0AD8CAA3C0A22D938C8324260642A14A3E26BC70
* 855A5FB329E1AF09!
* 4BDE762B45F30CB8!
* C063C9436FCC579D!
* 4149B59A3EA4DCEE!

TODO: I should deprecate old hardware keys.
GPG does not play nicely with multiple encryption subkeys

or;

```bash
gpg --recv-key 2351AD620C3D83F2A5A562E17B8C6179AA1CD475
gpg --recv-key 0AD8CAA3C0A22D938C8324260642A14A3E26BC70
gpg --encrypt --armor -r 2351AD620C3D83F2A5A562E17B8C6179AA1CD475 -r 0AD8CAA3C0A22D938C8324260642A14A3E26BC70 -r 855A5FB329E1AF09\! -r 4BDE762B45F30CB8\! -r C063C9436FCC579D\! -r 4149B59A3EA4DCEE\! whatever.txt
```


### Main Master Key
For both;
    * `chris@scutcher.uk`
    * `cscutche@cisco.com`

`ChrisScutcher-2351AD620C3D83F2A5A562E17B8C6179AA1CD475.asc`

Please use the above key for most applications.

I've got two types of hardware keys. The one I use for my main key
is basically a smartcard which is well supported everywhere but is less secure
and easier to accidentally lose than the one I use for my super secure key
below.

I actually have two of said hardware keys but, by default gpg will pick one of
them only, the newest, to use when sending me encrypted stuff.
That's why I manually specify the two encryption subkeys above.

### Super Secure Key
Uses email `secure@chris.scutcher.uk`.

`ChrisScutcher-SuperSecure-0AD8CAA3C0A22D938C8324260642A14A3E26BC70.asc`

This is also based on a hardware key.
It's a more advanced device and more secure in both terms of getting pwned, and
against me accidentally losing or destroying it.

However it's also a pain to use. It's physically larger, and the extra security
makes it slow to sign or decrypt.
It's also less well supported by tools that expect a more or less standard gpg
agent.

I use it when I want something super-duper extra secure.

### Old keys
I'm trying to remove all the old keys I have floating about.
There may be keys I've yet to revoke everywhere.
I'm working on it.

### Why is this all so complicated?

I don't freakin' know!

* For one thing gpg doesn't allow for multiple encryption subkeys automatically.
  It'll always use the last one created, which is irritating.
* Neither of the hardware keys I use are perfect and both have some pretty big
  drawbacks.  They both have a role they're better at so I'm using both which
  complicates things.
* GPG is just a bit of pain in the arse in general. I mean it's a powerful old
  beast, but how the hell a mere mortal is meant to understand all this stuff
  and use it in a way that's actually secure is beyond me.
